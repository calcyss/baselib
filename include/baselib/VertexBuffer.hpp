#ifndef VERTEXBUFFER_HPP
#define VERTEXBUFFER_HPP

#include<baselib/Shader.hpp>

namespace baselib
{
    /**
    * \brief Represents an OpenGL Vertex Buffer Object.
    */
    class VertexBuffer
    {
    public:
        VertexBuffer();
        ~VertexBuffer();

        unsigned int getHandle();
        void linkShader(Shader* item_shader);
        Shader* unlinkShader();
        bool isShaderLinked();
        void uploadData();
        void uploadSubData();
        void draw();
    private:
        Shader* m_Shader;
        bool m_HasShader;
        unsigned int m_Handle;
    };
}

#endif // VERTEXBUFFER_HPP
