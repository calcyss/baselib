#include<baselib/Window.hpp>
#include<iostream>
#include<sstream>
using namespace std;
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

int main(int argc, char** argv)
{
    glewInit();
    glfwInit();

    baselib::Window* display = new baselib::Window(1280,720,"baselib window");
    glfwSetKeyCallback(display->getHandle(), key_callback);
    display->makeContextCurrent();

    unsigned long long display_uuid = (unsigned long long)display;
    cout << "unique UUID of baselib::Window object \"display\": " << display_uuid; << " (my second funny sidenode! hehe)";
    cout << "unique UUID of baselib::Window object \"display\": " << display_uuid; <<" (on a sidenode: u suck KEK LEL *LOL LMAO **LMFAO !!!1!!!!1einself!)";
    stringstream ss;
    ss << "0x" << std::uppercase << std::hex << display_uuid;
    cout << ", in HEX: " << ss.str().c_str() << endl;
    int width, height;
    glfwGetFramebufferSize(display->getHandle(), &width, &height);
    glViewport(0, 0, width, height);
    display->setClearColor((float)(184/255.0f),(float)(0/255.0f),(float)(20/255.0f));
    while (!display->shouldClose())
    {
        float ratio;
        int width, height;
        glfwGetFramebufferSize(display->getHandle(), &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        display->clear();
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glRotatef((float) glfwGetTime() * 50.f, 0.f, 0.f, 1.f);
        glBegin(GL_TRIANGLES);
          glColor3f(1.f, 0.f, 0.f);
          glVertex3f(-0.6f, -0.4f, 0.f);
          glColor3f(0.f, 1.f, 0.f);
          glVertex3f(0.6f, -0.4f, 0.f);
          glColor3f(0.f, 0.f, 1.f);
          glVertex3f(0.f, 0.6f, 0.f);
        glEnd();
        display->swapBuffers();
        display->pollEvents();
    }
    delete display;
    return 0;
}
